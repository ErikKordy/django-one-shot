from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from todos.forms import TodolistForm, TodoItemForm
from .models import TodoItem


# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists,
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_object": todo_list,
    }
    return render(request, "todos/<int:pk>/delete/", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodolistForm(request.POST)
        if form.is_valid():
            TodoList_instance = form.save()
            return redirect("todo_list_detail", id=TodoList_instance.id)
    else:
        form = TodolistForm()

        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def update_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodolistForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodolistForm(instance=todolist)

        context = {
            "form": form,
        }
        return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem_instance = form.save()
            return redirect("todo_list_detail", id=todoitem_instance.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/item.html", context)


def update_todoitem(request, id):
    todoitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)

    context = {"form": form}

    return render(request, "items/edit.html", context)
