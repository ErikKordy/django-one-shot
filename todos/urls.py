from django.urls import path
from todos.views import (
    todo_list,
    show_todo_list,
    create_todolist,
    update_todolist,
    delete_todolist,
    create_todoitem,
    update_todoitem,
)

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todo_list, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("<int:id>/delete", delete_todolist, name="todo_list_delete"),
    path("todos/item/", create_todoitem, name="/todos/items/create/"),
    path("items/<int:id>/edit/", update_todoitem, name="todo_item_update"),
]
